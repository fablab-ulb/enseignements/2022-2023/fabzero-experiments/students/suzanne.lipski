# 5. Dynamique de groupe et projet final

Ce module s'est déroulé sur plusieurs séances.  

## 5.1. Première séance (2/3/23) : Analyse et conception de projet
**BUT** Créer un [arbre des problèmes et solutions](https://www.youtube.com/watch?app=desktop&v=9KIlK61RInY). Pour ce faire, il nous fallait prendre inspiration du [FabLab projects](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/#archives) et [frugal science projects](https://gitlab.com/fablab-ulb/enseignements/fabzero/projects/-/blob/main/open-source-lab.md) présenté en classe et choisir un projet qui nous tient à coeur. L'analyser, construire et commenter un arbre des problèmes et un arbre des solutions.    
Je me mets en binôme avec Eliot pour faire cette tâche. Nous décidons de parler de [**Low-Tech**](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn/fabzero-modules/module05/#:~:text=qui%20a%20suivi.-,Les%20low%2Dtech,-%23). 

### Etapes à suivre : 
Créer un arbre à problèmes  :
- étape 1 : identifier le **problème** principal (tronc)   
- étape 2 : identifier les causes principales du problème (racines) et les **causes** derrière les causes (racines plus profondes)  
- étape 3 : identifier le problème **conséquences** directes (branches) et effets secondaires (ramifications)

Transformez votre arbre à problèmes en arbre à objectifs :  
- étape 1 : reformuler le problème en **objectif**  
- étape 2 : remplacer les causes par les **activités** à développer pour atteindre l'objectif   
- étape 3 : reformuler les conséquences du problème en **résultats** du projet .

### L'arbre de problèmes 

Voir la [documentation de Eliot](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn/fabzero-modules/module05/)

![](https://imagizer.imageshack.com/img924/5489/KzcF0g.jpg)

### L'arbre des Solutions 

L'arbre des solutions se construit de la même manière que l'arbre des problèmes. 
![](https://imagizer.imageshack.com/img922/4899/V0YCnK.jpg)

Voici un arbre des solutions qui n'en contient que quelques-unes.

### Checkist du (2/3/23)
- [x] Inspirez-vous des projets de fablab et de projets de science frugale présentés en classe cette semaine et choisissez un projet qui vous tient à cœur.
- [x] Analysez le projet et construisez un arbre à problèmes et un arbre à objectifs. Affichez les diagrammes sur le module 5 de votre site Web et décrivez-le brièvement.

## 5.2 Deuxième scéance (7/3/23) : Formation de groupe et remue-méninges sur les problèmes du projet

### Avant le cours 
Il a été demandé de trouver et apporter en classe un objet qui représente un thème ou un enjeu de société qui vous tient à cœur. J'ai apporté une ***map monde***. Ce qui illustre plein de choses! Ma fascination pour la Terre, les océans (qu'on voit si bien de là-haut), le voyage, comment faire l'un sans empiéter sur l'autre ? 

### Pendant le cours
Emma avait un coquillage, Chiara un tableau cap-verdien fait avec des matériaux locaux (sable), Alicia un canard en plastique et Martin des algues. L'eau nous liait !  

Après discussion dirigée par Chloé, nous avons trouvé notre thème : **Vivre en harmonie avec les Océans** 
 
Voici une liste de problématiques auxquelles nous avons pensé ; 
![](https://imagizer.imageshack.com/img922/4638/71tcE7.jpg)


### Checklist du (7/3/23) : 
-  [x] Décrivez le processus de remue-méninges que vous avez suivi pour passer de votre objet à un groupe, un thème et un ensemble de problèmes liés à des projets possibles. Il n'y a pas de bon ou mauvais ici !
- [x] Décrivez l'objet que vous avez choisi et énumérez le thème et les problèmes possibles identifiés par le groupe.

## 5.3 Troisième scéance (28/3/23) : Dynamique de groupe 
Durant ces 2 heures, nous avons appris à utiliser des outils pour mieux fonctionner en groupe. Plus précisément, nous avons appris  à structurer nos réunions, à définir et attribuer des rôles, à utiliser des techniques de prise de décision, à communiquer entre vous, à donner la parole à chacun et à fournir des commentaires.   
Voici une photo du tableau, récapitulatif de la séance :   
![](https://imagizer.imageshack.com/img923/2504/m0sSRH.jpg)  

### 3 outils 
Il nous a été demandé de documenter et commenter 3 outils qui nous ont parlé lors de cette séance :   

#### 1. ***L'art de décider***

L'art de décider peut être pratiqué **seul**, **en groupe** ou **pas**, et chaque contexte de décision présente ses propres avantages et défis.

Lorsque l'on prend des décisions seul, il est important de s'assurer que toutes les options sont examinées de manière critique et impartiale. Les avantages de prendre des décisions seul sont la rapidité de la prise de décision et l'autonomie dans le processus de décision. Cependant, il est important de prendre en compte les biais cognitifs personnels qui peuvent influencer la prise de décision et de chercher à obtenir des commentaires et des perspectives externes pour valider la décision.

Lorsque la prise de décision se fait en groupe, cela peut apporter une diversité de perspectives et d'expériences pour aider à identifier des options qui pourraient ne pas être évidentes pour une personne seule. Cependant, le processus de décision en groupe peut prendre plus de temps et nécessiter la gestion de conflits et de divergences d'opinions pour arriver à un consensus ou à une décision finale.

Enfin, il peut arriver que la décision ne soit pas nécessaire ou que les coûts de la prise de décision soient trop élevés par rapport aux avantages. Dans ces cas-là, il peut être préférable de ne pas prendre de décision du tout ou de la reporter à un moment où des informations supplémentaires ou des perspectives différentes seront disponibles.

En somme, l'art de décider dépend du contexte et des objectifs de la prise de décision, et il est important de considérer les avantages et les défis de chaque approche pour arriver à une décision éclairée et efficace.  

Ce principe m'a directement parlé. C'est un problème auquel je fais souvent face, en groupe de travail, mais aussi dans ma vie personnelle (ex : partir en vacances avec des amis), ce qui m'a permise d'intégrer assez intuitivement ce concept.   

#### 2. ***Doigt***
La technique du doigt consiste à faire la file dans le fil de la conversation. Je m'explique. Lève ton doigt quand tu as quelque chose à dire. Quand tu as quelque chose à dire et qu'une personne à déjà son doigt levé, tu en lèves 2. Et ainsi de suite...  
Cette technique permet de laisser tout le monde parler sans qu'une personne ne monopolise la parole. Elle peut s'avérer très utile pour peu que le groupe de discussion ne soit pas trop petit ou trop grand.   

#### 3. ***Température*** 
Quand quelqu'un parle et que tu éprouves un sentiment clair et concis comme "je suis d'accord", "je ne suis pas d'accord" ou encore "je suis mitigé.e", alors la technique de la température est faite pour toi!  
Il suffit de définir des gestes pour ces 3 états avec ton groupe. Ceci permet au groupe d'avoir une vision d'ensemble des différents avis du groupe sans avoir à prendre la parole.
La technique de la température a déjà fait ses preuves dans notre groupe ! 

### Checklist du (28/3/23)
- [x] Développez et documentez 3 outils pour la dynamique de groupe que vous avez appris et expliquez comment vous pourriez les utiliser à l'avenir.
- [] suite : <span style="color:orange">voir page gitlab du groupe 7 </span>


## 5.4 Quatrième scéance (29/3/23) : Problème et objectif de votre projet de groupe 
[<span style="color:orange">voir page gitlab du groupe 7 </span>](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/groups/group-07)

### Checklist du (29/3/23)
[<span style="color:orange">voir page gitlab du groupe 7 </span>](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/groups/group-07)
