# 1. Gestion de projet et documentation

Ici se trouve ma toute première documentation. Elle comporte les premières étapes de mon apprentissage : introduction à Gitlab, Git bash, clef SSH, markdown (visual studio),...
Étant novice dans le domaine, je vais tenter de définir et décrire mes actions (lorsque je les comprends moi-même (; )

## 1. Notes d'introduction

2 premières heures, Denis **présente** et j’en retiens :    
- Consulter le travail déjà fait des élèves des années précédentes, et ceux qui m’entourent. Demander de l’aide autour de moi.    
- La documentation sert à garder une trace écrite de mes acquis et de mon avancement. Par la suite, elle pourrait servir entre autres de rappel.   
- Fablab est un espace où la créativité, l’initiative, l’investissement et l’autonomie règnent, on nous encourage à sortir de notre zone de confort, à se poser des questions et à s’entraider.     
- Les 2 h qui vont suivre serviront à :   
  - Travailler en suivant un tuto git   
  - Utiliser git pour construire un site personnel qui me décrit dans l'archive de la classe   
  - Tester et appliquer les principes d'un project managment tout au long de la classe    
  - Documenter mon apprentissage de ce module 



## 2. En autonomie 

Les 2 h suivantes seront les premiers instants en autonomie. Je découvre Bash et Linux, parcours le tutoriel FabZéro (premier contact avec bash shell), openclassrooms - Introduction au scripts shell (frensh) (inscription a openclassrooms). 
J'avance lentement, c'est frustrant de ne pas comprendre ce que je fais. D'ailleurs il me faudra une semaine de plus et plus d'aide de la part de mes camarades pour enfin arriver à accomplir toutes les tâches de ce premier module.  

### 2.1 Liens utiles
- [documentation](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#git) 
- [Markdown](https://en.wikipedia.org/wiki/Markdown)

### 2.2. Définitions 
Beaucoup de mots et concepts nouveaux. Voici ce que je retiens suite à mes interactions avec Denis, mes camarades et Internet. 

- [**FabLab**](https://fr.wikipedia.org/wiki/Fab_lab) 
Contraction de l'anglais « laboratoire de fabrication ») est un [*makerspace*] (https://fr.wikipedia.org/wiki/Makerspace) cadré par MIT et la FabFoundation en proposant un inventaire minimal permettant la création des principaux projets fab labs, un ensemble de logiciels et solutions libres et *open-sources*, les Fab Modules, et une charte de gouvernance, la [Fab Charter](http://fab.cba.mit.edu/about/charter/) . Le logo traduit les 3 fondements du projet : modèle commercial et économique, impact social et durabilité et recherche et éducation. Les fab labs sont réunis en un réseau mondial très actif.
Le fablab fonctionne par formations permettant d’accéder à l’utilisation des machines. 
Le fablab ULB est ouvert à tous et son objectif est de promouvoir de nouveaux usages au sein de l’université. Les FabLabs fournissent un accès global aux moyens modernes d’invention.  
- [**Documentation**](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#git)  Expliqué plus simplement plus haut, la documentation sert à avoir une trace : du processus d'apprentissage (échecs et succès) et des choses apprisent (pour futur Suzanne et camarades de Suzanne). Pour ce faire, il faut avoir un contenu simple et léger, fait en temps réel à l'aide de textes, images, vidéos et liens. 
- **Git**[^1] Git est un [logiciel de gestion de versions](https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions) décentralisé. C'est un logiciel libre et gratuit. Il s’agit du logiciel de gestion de versions le plus populaire dans le développement logiciel et web, qui est utilisé par des dizaines de millions de personnes, sur tous les environnements (Windows, Mac, Linux).
  - **GitLab** est une service web d'hébergement et de gestion de développement de logiciels. 
  - **Git Bash** Bash, acronyme de Bourne-Again shell est un interpréteur en ligne de commande de type script. 
- **Bash Shell** 
- **Paire de Clefs SSH**, acronyme de Secure Shell, servent à authentifier les utilisateurs sans utiliser de noms d’utilisateur ni de mots de passe. Au lieu de cela, les utilisateurs disposent d’une paire de clés SSH de confiance qui les authentifie comme la personne qu’ils disent être. Chaque paire de clés est composée d'une clé publique (capable d'encrypter) et d'une clef privée (capable d'encrypter et de décrypter). L'utilisateur doit conserver la clé privée qui doit rester absolument secrète.    
La clef publique <span style="color:green">en vert </span> et la clef <span style="color:red">en rouge </span> privée  
![](https://imagizer.imageshack.com/img922/4421/YuI1Mu.jpg)

- [**Markdown**](https://en.wikipedia.org/wiki/Markdown) est un langage (comme python). Il a été créé dans le but d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être converti en HTML, en PDF ou en d'autres formats. 
- **Visual Studio Code** est un éditeur de code extensible. Git y est intégré. Il prend presque tous les principaux langages de programmation, nous allons utiliser markdown.  

### 2.3. Déroulement chronologique de mes actions 

#### 1. Installation de Git Bash 
#### 2. Paire de clefs SSH   
Je tente de suivre ce [tuto GitLab](https://docs.gitlab.com/ee/user/ssh.html). J'avance lentement et c'est frustrant de ne pas comprendre ce que je fais. Suite à des discussions avec Denis et plusieurs de mes camarades (patients !), j'en viens à choisir de créer une paire de clef ssh de type ED25519, je procède comme ci-dessous :   
 - Dans mon terminal écrire : `ssh-keygen -t ed25519 -C "suzanne.lipski@ulb.be"`    
 - Ensuite, j'introduis mon mot de passe --> ma clef est crée.    
 - Je copie ma clef publique (qui finit par .pub) trouvée dans mes documents.     
 - Sur Gitlab je vais dans **Préférences --> SSH keys --> coller ma clef publique --> add key**     
 - Pour ajouter ma clef SSH à mon compte GitLab, il va falloir que je clone ma clef ; je copie-colle ![](https://imagizer.imageshack.com/img922/7223/74Yfyn.jpg) et l'introduit dans mon terminal.  


#### 3. Visual Studio   
Après avoir téléchargé Visual studio code, je m'informe sur Markdown entre autre dans [ce guide](https://www.markdownguide.org/getting-started/), [celui-ci](https://www.markdownguide.org/cheat-sheet/) et [celui-là](https://www.plpeeters.com/blog/fr/post/73-le-markdown-un-formidable-outil), j'en ressors quelques fondamentaux : 

| Commande                           |  Description    | 
|------------------------------------|-----------------|
| `# Titre`                            |# Titre |  
| `## Sous-Titre`                       | ## Sous-Titre  | 
| `**Gras**`                         | **Gras** |
| `*Italique*`                        | *Italique*  |  
| `1. point 1 de la liste \n 2. point 2 de la liste`                       | 1. point 1 de la liste \n 2. point 2 de la liste  | 
| `Passage[^2] à annoter`                         | Passage[^2] à annoter |
| `[^2]: note de bas de page`                        | [^2]: note de bas de page  |  
| `***Gras et Italique***`                         | ***Gras et Italique***  |  
| `![Titre de l'image](exemple.jpg)`| ![Image](https://imagizer.imageshack.com/img923/7880/aUMvFw.jpg) |
| `[Tamino qu'il est beau!] (https://www.youtube.com/watch?v=7mXd6URFqd8)`  | [Tamino qu'il est beau!](https://www.youtube.com/watch?v=7mXd6URFqd8)  |   


L'**ajout d'images** a longtemps été un problème. Le fait qu'il y ai plein de façons de faire m'a perdue. Je décide donc d'utiliser [ImageShack](https://imageshack.com/my/images) (proposé par Martin), qui me permettra de créer un lien à partir de mes images pour ensuite pouvoir les ajouter à ma documentation. Coté positif, c'est que c'est facile d'utilisation, cependant, la qualité d'image n'est pas dingue (pas très grave) et en plus après 1mois, c'est payant ;(. 


#### 4. Réalisation de mon propre website     
Grâce à tout ce que j'ai appris, je peux maintenant modifier mon site. Je vais changer le thème dans index, écrire un peu plus sur moi,... Par la suite j'écrirai ma docu des modules. Après avoir fait les modifications locales de mon site sur Visual Studio Code, il me faut les publier sur GitLab. Pour ce faire, j'apprends quelques commandes : 


| Commande                           |  Description    | 
|------------------------------------|-----------------|
| `cd`                              |ouvrir l'emplacement où se trouve le dossier où les modifications ont été opérées |  
| `git pull`                         | Télécharger la dernière version du projet   | 
| `gitt add -A`                      | Ajouter tous les changements |
| `git commit -m "titre de modif"`   | Accompagner d'un message d'explications des changements effectués  |  
| `git push`                         | Envoyer nos changements sur le serveur  |  

Je repeterai ce processus (ci-dessous), pour publier mes modifs: 

```  
- cd    
- cd suzanne.lipski    
- git status    
- git add -A    
- git commit -m "titre de modification"   
- git push     
```

J'apprends aussi qu'il est inutile de convertir en HTML après avoir écrit en markdown, ça se fait automatiquement. 

#### 5. Utilisation des principes du project managment

2 sources nous ont été proposées [celle-ci](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#as-you-work-documentation) et [celle-là](https://dtwg4.github.io/blog-flexible-jekyll/techniques-gestion-projet/) , écrite par Denis. Après une présentation et une relecture minucieuse, voici quelques notes qui pourraient s'avérées m'être utiles pour éviter d'être débordée et me sentir submergée :      

1. **Trial & Modular & Hierarchical Planning** *Décomposer* mes tâches puis les *hiérarchiser* par ordre d'importance.   
2. **Supply-Side Time Management** Se concentrer sur la *gestion du temps* et non la gestion des tâches   
3. **Spiral Development** Réaliser le plus rapidement possible, une première couche de travail, un premier projet minimaliste fonctionnel. Pour ensuite rajouter d'autres couches, d'autres précisions au projet.     
4. **As-You-Work Documentation** documenter au fur et à mesure que j'apprends.    
5. En dernier recours ; effectuer un triage. Reporter ou annuler les tâches s'il ne reste pas suffisamment de temps. 

[^1]: *Origine du mot git* Le magazine PC World nous apprend que « quand on lui a demandé pourquoi il avait appelé son logiciel “git”, qui est à peu près l'équivalent de “connard” en argot britannique17,18, Linus Torvalds a répondu “je ne suis qu'un sale égocentrique, donc j'appelle tous mes projets d'après ma propre personne. D'abord Linux, puis Git.”19 »


## 3. Checklist du module 
- [x] work through a git tutorial   
- [x] use git to build a personal site in the class archive describing you.  
- [x] test and apply project management principles all along the class.  
- [x] document your learning path for this module.    
- [x] Evaluate your work (template)
 - [x] made a website and describe how you did it
 - [x] introduced yourself
 - [x] documented steps for uploading files to the archive
 - [x] documented steps for compressing images and keeping storage space low
 - [x] documented how you are going to use project management principles
 - [x] pushed to the class archive