# 3. Impression 3D
Ce module est la suite de celui qui le précède. Nous verrons comment imprimer le modèle réalisé au cours passé grâce aux logiciels de slicing et les imprimantes 3D! 

## 1. Notes d'introduction

**Avant le cours** je télécharge [slicing software](https://en.wikipedia.org/wiki/Slicer_(3D_printing)) , [PrusaSlicer](https://help.prusa3d.com/article/install-prusaslicer_1903) et je parcours ce [tutoriel](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) sur l'imprimante 3D. 

Durant les 2 premières heures, **présentation** :    
- Imprimante 3D   
- [Torture test](https://www.thingiverse.com/thing:2806295)    
- [Design rules](https://help.prusa3d.com/article/modeling-with-3d-printing-in-mind_164135)    
- [Materials](https://blog.prusa3d.com/advanced-filament-guide_39718/)   
- [CNC Kitchen Youtube channel](https://www.youtube.com/@CNCKitchen)    
   

## 2. En autonomie 
### 2.1. Définitions    
- [***Slicer***](https://en.wikipedia.org/wiki/Slicer_(3D_printing)) aussi appelé logiciel de découpage en tranches ou **slicing software**, est un logiciel utilisé dans la majorité des processus d'impression 3D pour la conversion d'un modèle d'objet 3D en instructions spécifiques pour l'imprimante. En particulier, la conversion d'un modèle au format STL en commandes d'imprimante au format g-code dans la fabrication de filaments fondus et d'autres processus similaires. 

Au FabLab, c'est le logiciel [**PrusaSlicer**](https://www.prusa3d.com/fr/page/prusaslicer_424/) qui est utilisé. PrusaSlicer est un logiciel conçu pour faciliter le découpage (slicing) des modèles 3D pour l'impression 3D. Il est libre d'utilisation et permet de préparer les modèles 3D pour l’impression en générant les instructions de découpage (gcode) que l’imprimante 3D utilise pour fabriquer l’objet. . En utilisant PrusaSlicer, il est possible de personnaliser divers paramètres d'impression, tels que l'épaisseur de couche, la densité de remplissage, la vitesse d'impression, la température d'extrusion et la rétractation, pour obtenir des résultats optimaux. Le logiciel offre également des fonctionnalités avancées, telles que la génération de supports personnalisés pour les parties qui nécessitent un soutien pendant l'impression, la mise en file d'attente de plusieurs modèles d'impression et la simulation d'impression pour prévisualiser le résultat final. Les imprimantes compatibles avec PrusaSlicer sont les imprimantes Prusa MK3/MK3+, qui sont auto-réplicatives, c'est-à-dire qu'elles peuvent imprimer les différents composants matériels de la machine.


### 2.2. Déroulement chronologique de mes actions 
- ***Préparation du fichier*** Voir [module 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/fabzero-modules/module02/) : Créer un fichier stl
- ***Installation de PrusaSlicer*** 
- Importer le fichier stl dans PrusaSlicer
- Pour assurer une impression optimale, l'objet doit être centré et la surface la plus grande doit être en contact avec le plateau.
- ***Impression*** : les imprimantes présentes au FabLab sont des imprimantes 3D Prusa MK3. Les paramètres d'impression, tels que le filament, l'imprimante, les supports et l'épaisseur de couche, doivent être ajustés. 
- En découpant l'objet, on peut observer les différentes couches telles qu'elles seront imprimées.
- Une fois que les réglages sont terminés, il est possible d'exporter le fichier gcode.
- Enfin, il suffit de placer le fichier gcode sur une carte SD pour que l'imprimante puisse l'utiliser.
- Avant de faire l'impression de mon objet, je réalise quelques tests, pour comparer la grandeur de mes paramètres OpenScad et ce qui est réellement imprimé.    
![](https://imagizer.imageshack.com/img924/8059/L6vWz9.jpg)
![](https://imagizer.imageshack.com/img924/1748/S0R5tr.jpg)
![](https://imagizer.imageshack.com/img922/475/FkOyGV.jpg)

### 2.3. Kit 
Pour la création du Kit, Emma, Eliott, Martin et moi avons fait équipe. Après de trop longues discussions, rires, pleurs, déchirement et de réconciliation, nous en sommes arrivés à faire une catapulte atypique! Martin a rélisé l'assemblage, la [vidéo](https://www.youtube.com/shorts/2lgtvRBjNhY) et à accepté d'impliquer son légo. Voici le résultat : 
![](https://imagizer.imageshack.com/img924/4348/8FsE6Y.jpg)

## 3. Commentaire
Ci-dessous, quelques remarques suite à mon utilisation de l'imprimante 3D, 


| Avantages                             |  Limitations      | 
|------------------------------------|-------------------------------------------------------------------------------|
| Rapidité                           | Logiciels difficiles à appréhender, leur technicité est donc une limite |  
| Coût                               | Matériaux inaccessibles, à titre de particulier, matériaux et dimensions limités  | 
| Créativité sans limites, fabrication pour tout type de domaines (santé, agroalimentaire, construction, automobile, bricolage,...)                    | Où est ce qu'on pose la limite éthique? ex : création d'armes |
| Réduction des risques, en apportant tous les changements nécessaires à un design avant de consentir à son impression                      | Problème de droit de la propriété intellectuelle, duplication illégale|  

 

## 4. Checklist du module 
- [x] Explained what you learned from testing the 3D printers  
- [x] Explained how you have identified your design parts parameters  
- [x] Shown how you made your kit with words/images/screenshots  
- [x] Included your original design files for 3D printing (both CAD and generic (STL, OBJ or 3MF) files)  
- [x] Included images and description of your working mechanism.