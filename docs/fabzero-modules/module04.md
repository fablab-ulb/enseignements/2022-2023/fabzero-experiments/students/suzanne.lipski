# 4. Outil Fab sélectionné : Découpe laser

Pour ce module, j'ai été initiée à l'utilisation d'une découpe laser. Pour ce faire, j'ai appris à préparer des fichiers CAO 2D et à les découper au laser.  

## 1. Introduction 
***Avant le cours***, je relis le [tutoriel de base d'Inkscape](https://inkscape.org/learn/tutorials/) (rappel du [module 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/fabzero-modules/module02/)), ensuite et le plus **important** j'étudie [ce manuel de découpe laser](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut). J'en retiens que certains matériaux sont conseillés, d'autres déconseillés et d'autres interdit. Mais aussi, par précaution, il faut toujours : 
*savoir avant utilisation* :   
- activer l'air comprimé    
- allumer l'extracteur de fumée  
- rester à proximité de la machine jusqu'à la fin de la découpe  
- savoir où est le bouton d'arrêt d'urgence  
- savoir où se trouve l'extracteur au CO2
*pendant utilisation*  
- Ne jamais quitter la pièce si le laser est activé (bouton pause)  
- Porter des lunettes protectrices si besoin de regarder le laser (pour un long moment)
![](https://imagizer.imageshack.com/img923/8864/We2BVn.jpg)   
- Ne jamais ouvrir la machine en cours d'utilisation et attendre quelques instants à la fin (ex:le filtre de la machine Lasersaur était fortement encrassé, il a fallu attendre quelques minutes avant d'ouvrir)

Après avoir tous réussi le test d'entrée, un FabLabeur nous fait ***une présentation*** des différentes machines ; 

1. [Epilog Fusion Pro 32](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md)
2. [Lasersaur](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md)
3. [Full Spectrum Muse](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6)   
   
     
     Fromat du fichier pour la découpeuse `.svg`   

## 2. Laser, pour quoi faire?
Le laser peut servir à   
- **Découpe (vector)**, La découpe suit un tracé vectoriel, ce qui nécessite l'importation d'un fichier vectoriel (par exemple au format .svg). Pour les matériaux épais, plusieurs passages sont souvent nécessaires pour les découper complètement.   
- ***Gravure vectorielle***La technique utilisée est similaire à celle de la découpe, mais avec une puissance réduite. Les traits sont donc gravés à une faible profondeur.   
- **Gravure matricielle (engrave)** La technique de gravure matricielle consiste à graver un dessin à la surface du matériau. Dans ce processus, la machine fonctionne comme une imprimante et le dessin est imprimé sous forme de trame. Pour obtenir cet effet, la machine ajuste l'intensité du laser en fonction de la luminosité du dessin. Ainsi, les parties sombres seront gravées plus profondément que les parties claires.

En résumé, ces trois effets recherchés sont de même nature. Tout dépend juste du **matériaux**, de **la puissance** et de la **vitesse**[^1] utilisé. [Voici un document récapitulatif.](https://www.epiloglaser.com/assets/downloads/fusion-material-settings.pdf)


## 3. Déroulement chronologique de mes actions 
- Avec Lucie, nous sélectionnons un modèle qui nous plaît sur [ce site](https://www.festi.info/boxes.py/). 
- Nous voulions apporter quelques modifications au produit proposé pour créer un ***porte épices***. Nous avions le choix, soit le faire directement dans les paramètres du site, soit télécharger le modèle et faire des modifications sur Inkscape, chose que nous avons fait.   
Voici le modèle de base : 
![](https://imagizer.imageshack.com/img924/148/EeGGdW.png)   
![](https://imagizer.imageshack.com/img923/9163/FzHsOn.png)
Voici ce qu'on en a fait : 
![](https://imagizer.imageshack.com/img924/5440/FNpggo.png)
On a ajouté du texte "HE PISSE" qu'on a vectorisé sur Inkscape. 
- on a attendu qu'une imprimante se libère pour pouvoir imprimer notre création. On a donc utilisé la [***Lasersaur***](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md). Elle a été développé par le Fablab, elle fonctionne très bien, bien que bricolée. Il faut donc ouvrir manuellement l'extracteur de fumée, la vanne d'air comprimé et le refroidisseur à eau. La taille de notre objet était relativement petite, et cette machine est faite pour travailler sur de grandes surfaces (si c'était à refaire j'aurais utilisé une des 2 autres machines).   
  
Suite à un test effectué sur notre matériel avec différentes puissances et différentes vitesses, nous avons réglé les vitesses et puissances idéales pour d'une part graver le "HE PISSE" et découper les contours.  
![](https://imagizer.imageshack.com/img923/6781/8Ag8hO.jpg)
![](https://imagizer.imageshack.com/img923/1926/xcb1R3.jpg)  
On apprend également que pour que le travail soit bien fait, il est conseillé de commencer par la gravure puis la découpe (pour éviter que le matériel ne bouge).   
Voici le résultat final!: 
![](https://imagizer.imageshack.com/img922/4949/BYpgep.jpg)

## 4. Checklist du module

- [x] Caractérisez la mise au point, la puissance, la vitesse, le trait de scie de votre découpeuse laser, … pour plier et découper du papier cartonné
Mission individuelle :
- [x] Concevoir, découper au laser et documenter un kirigami (couper et plier) pour créer un objet 3D
- [x] Démontrer et décrire les processus de conception 2D
- [x] Identifier et expliquer les processus impliqués dans l'utilisation de la découpeuse laser.
- [x] Développer, évaluer et construire l'objet 3D réalisé à partir d'un kirigami

[^1] :  Attention à ne pas confondre puissance et vitesse !   
- Une puissance élevée implique plus de chance de découper la matière en une passe, mais aussi plus de risque de brûler la matière !   
- Une vitesse élevée implique que le travail sera fini plus vite, mais aussi un risque de ne pas couper à travers la matière. (exemple : matériau épais et dense (ex : bois), on choisira une puissance élevée avec une faible vitesse. Pour un matériau fin (ex : papier/carton), on choisira une puissance faible avec une vitesse élevée.)

