# 2. Conception Assistée par Ordinateur (CAO)

Cette semaine, j'ai découvert le concept de Compliant mechanism ou méchanisme souple, mais aussi différents logiciels pour arriver à modéliser une idée (entre autres un Compliant mechanism) pour ensuite pouvoir (par la suite), l'imprimer à l'aide d'une imprimante 3D. 

## Notes d'introduction 
**Avant le cours** je télécharge Inkscape, OpenSCAD et FreeCAD.
J'ai aussi regardé [cette vidéo](https://www.youtube.com/watch?v=97t7Xj_iBv0), que j'ai trouvé passionnante, rien de mieux que pour découvrir une nouvelle notion! 

Pour ce deuxième module, nous commençons par 2h de **présentation** puis 2h de pratique. 
Dans cette unité, on a appris à :   
- En savoir plus sur le mécanisme et les flexions conformes   
- Évaluer et sélectionner un logiciel 3D    
- Démontrer et décrire les processus utilisés dans la modélisation avec un logiciel 3D    
- En savoir plus sur les licences de code et comment les utiliser.   
- Construire sur le code d'autres personnes et donner des crédits appropriés

Toutes ces choses sont encore inconnues pour moi. 


## En autonomie 


### Définitions
- [***Conception Assistée par Ordinateur ou CAO***](https://fr.wikipedia.org/wiki/Conception_assist%C3%A9e_par_ordinateur) (**ou en anglais computer aided disign ou CAD**) comprend l'ensemble des logiciels et des techniques de modélisation géométrique permettant de concevoir, de tester virtuellement – à l'aide d'un ordinateur et des techniques de simulation numérique – et de réaliser des produits manufacturés et les outils pour les fabriquer.
Il s'agit d'un outil informatique souvent lié à un métier, fonctionnant en langage dit **objet**, et permettant l'organisation virtuelle de fonctions techniques. Cela permet ensuite la simulation de comportement de l'objet conçu, l'édition éventuelle d'un plan ou d'un schéma étant automatique et accessoire.   

 -  [***InkSacpe***](https://inkscape.org/fr/) est un logiciel de dessin vectoriel permettant la création de **dessins 2D**. C'est un editeur de graphiques vectoriels gratuit et open source. Souvent utilisé pour les **illustrations artistiques** et techniques telles que les dessins animés, les images clipart, les logos, la typographie, les diagrammes et les organigrammes.
  Il utilise des graphiques vectoriels pour permettre des impressions et des rendus nets à une **résolution illimitée** et n'est pas lié à un nombre fixe de pixels. Inkscape utilise le format de **fichier SVG** standardisé comme format principal, qui est pris en charge par de nombreuses autres applications, y compris les navigateurs Web.
  Il peut importer et exporter divers formats de fichiers, notamment SVG, AI, EPS, PDF, PS et PNG. Il dispose d'un ensemble complet de fonctionnalités, d'une interface simple, d'un support multilingue et est conçu pour être extensible. Les utilisateurs peuvent personnaliser les fonctionnalités d'Inkscape avec des modules complémentaires . Inkscape est membre de Software Freedom Conservancy, une organisation à but non lucratif. Inkscape a de **nombreux auteurs**, chaque auteur conserve son propre droit d'auteur et les auteurs sont également impliqués dans la définition des objectifs techniques et du projet d'Inkscape. Il existe également de nombreux autres contributeurs non-codeurs qui sont considérés comme des éléments importants du projet Inkscape.  

  - [***OpenSCAD***](https://openscad.org/) est un logiciel libre qui permet de créer des **modèles 3D**. C'est un logiciel gratuit et disponible pour Linux/UNIX, Windows et Mac OS X. Contrairement à la plupart des logiciels gratuits de création de modèles 3D (comme Blender), il ne se concentre pas sur les aspects artistiques de la modélisation 3D mais plutôt sur les **aspects CAO**. Ainsi, c'est peut-être l'application que vous recherchez lorsque vous envisagez de créer des **modèles 3D de pièces de machine**, mais ce n'est certainement pas ce que vous recherchez lorsque vous êtes plus intéressé par la création de films d'animation par ordinateur. OpenSCAD n'est pas un modélisateur interactif. Au lieu de cela, c'est quelque chose comme un compilateur 3D qui lit dans un fichier de script qui décrit l'objet et rend le modèle 3D à partir de ce fichier de [script](https://openscad.org/cheatsheet/). Cela vous donne (le concepteur) un contrôle total sur le processus de modélisation et vous permet de modifier facilement n'importe quelle étape du processus de modélisation ou de créer des conceptions définies par des paramètres configurables. OpenSCAD fournit deux techniques de modélisations principales: premièrement, il y a la **géométrie solide constructive** (alias CSG) et deuxièmement, il y a l'**extrusion de contours 2D**. Les fichiers Autocad DXF peuvent être utilisés comme formats d'échange de données pour ces contours 2Ds. En plus des chemins 2Ds pour l'extrusion, il est également possible de lire les paramètres de conception à partir de fichiers DXF. Outre les fichiers DXF, OpenSCAD peut lire et créer des modèles 3Ds aux formats de fichier STL et OFF.  

  - [***FreeCAD***](https://www.freecad.org/) "fait pour construire pour le monde réel". Tout ce qui est fait dans FreeCAD utilise des unités du monde réel, qu'il s'agisse de microns, de kilomètres, de pouces ou de pieds, ou même de toute combinaison d'unités. FreeCAD offre des outils pour produire, exporter et éditer des modèles solides de haute précision, les exporter pour **l'impression 3D ou l'usinage CNC**, créer des dessins et des vues 2Ds de vos modèles, effectuer des analyses telles que des analyses par élément fini ou exporté des données de modèle telles que des quantités ou nomenclatures de matériaux. Ce logiciel dispose d'un moteur de géométrie avancé basé sur la technologie [**Open CASCADE**](https://fr.wikipedia.org/wiki/Open_CASCADE_Technology). Il prend en charge les solides, les objets de représentation des limites (BRep) et les courbes et surfaces de splines à base rationnelle non uniforme (NURBS), et offre une large gamme d'outils pour créer et modifier ces objets, y compris les opérations booléennes complexes, les congés, le nettoyage de forme et bien plus encore. Tous les objets FreeCAD sont **nativement paramétriques**. La fonctionnalité de base de FreeCAD est codée en C++. Une grande partie des couches externes, des ateliers et presque toute la communication entre le noyau et l'interface utilisateur est codée en Python. FreeCAD vous permet d'importer et d'exporter des modèles et de nombreux autres types de données à partir de vos modèles, tels que des résultats d'analyses ou des données de quantités, vers des dizaines de formats de fichiers différents tels que STEP , IGES , OBJ , STL , DWG , DXF , SVG , SHP , DAE , IFC ou OFF , NASTRAN , VRML , OpenSCAD CSG et bien d'autres, en plus du format de fichier FCStd natif de FreeCAD. FreeCAD est fait pour tout le monde, par tout le monde. 

- [***FlexLinks***](https://www.compliantmechanisms.byu.edu/flexlinks) est un type de compliant mechanism. Les FlexLinks ont été développés pour un prototypage rapide de preuve de concept de mécanismes conformes. Combinés avec des composants commerciaux du groupe LEGO, des composants conformes personnalisés peuvent être utilisés pour construire une large gamme de mécanismes conformes. Cette page comprend des fichiers qui peuvent être téléchargés pour créer un certain nombre de composants conformes personnalisés compatibles avec les composants LEGO. Deux formats de fichier sont fournis pour chaque composant : dxf et vnc.  

- [***Creative Commons open-source Licence***](https://creativecommons.org/about/cclicenses/) constitue un ensemble de licences régissant les conditions de réutilisation et de distribution d'oeuvres. C'est une organisation à but non-lucratif, dont le but est de faciliter la diffusion et le partage des oeuvres, tout en accompagnant les nouvelles pratiques de création à l'ère numérique.   
Voici un schéma qui illustre les différentes licences ; 
![voici un schéma qui illustre les différentes licences](https://imagizer.imageshack.com/img924/7513/OQiEkH.jpg)  

- Coder de manière ***paramétrique***. C’est-à-dire qu’au lieu de mettre directement les valeurs souhaitées dans le code, nous définissons des paramètres et utilisons ceux-ci dans le code. Cela permet de pouvoir facilement changer les valeurs de certains paramètres sans devoir récrire tout le code. 

Il existe également une librairie contenant plein de codes pour des formes géométriques plus complexes. Il s’agit de BOSL.

### Déroulement chronologique de mes actions

1. Je choisis d'utiliser **OpenScad** pour coder le modèle que je choisirai par la suite.
2. Je parcours FlexLink, pour trouver un design qui me plait. 
![](Animation2.gif)
3. En m'aidant de [ce tuto](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/OpenSCAD.md) et [ce cheatsheet](https://openscad.org/cheatsheet/), mais surtout de mes camarades toujours aussi patients ! Je parviens à coder mon modèle :      

L'avantage de la modélisation paramétrique pourrait me permettre d'ajouter les dimensions exactes recherchées par la suite. Je décide tout de même de mettre les bonnes dès le début. Je me base donc sur les dimensions des lEGOS   
![](https://imagizer.imageshack.com/img923/9838/vGFCsc.jpg)  

Suite à de nombreux essais, je n'arrive pas à coder seule ma pièce, je m'inspire de ce qu'ont fait mes camarades et ceux des années précédentes.  
- [Lien](https://www.thingiverse.com/thing:3020736) du Flexlink qui a inspiré ma pièce (.slt)  (License FlexLinks: Fixed-Fixed Straight Beam (LEGO Compatible) byBYU_CMRis licensed under theCreative Commons - Attributionlicense.)  

Voilà le résultat :   
![](https://imagizer.imageshack.com/img922/4736/Md957r.jpg)
Le code est paramétrique.


## Checklist du module
- [x] Design, model, and document a parametric FlexLinks construction kit that you will fabricate next week using 3D printers.  
- [x] Make it parametric, so you will be able to make adjustments accounting for the material properties and the machine characteristics.  
- [x] Read about Creative Commons open-source Licence and add a CC license to your work. 
- [x] Complete your FlexLinks kit that you made by fetching the code of at least 1 part made by another classmate during this week. Acknowledge its work. Modify its code and get the parts ready to print.
