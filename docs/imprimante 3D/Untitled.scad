$fn = 100;

// Parameters 
num_holes = 2;
radius = 2.5;
hole_separation = 3.2; 
hauteur = 9.6;
thinckness = 7.8;
longueur = 15.8;
bloque_dim = [longueur, thinckness, hauteur] ;
longueurl = 50 ;//longueur de la languette
thincknessl = 1.8 ; 


//
module bloque(longueur, thinckness, hauteur){
    cube([longueur, thinckness, hauteur]);}
    bloque(longueur, thinckness, hauteur);
    

module trous(radius, num_holes, hole_separation){
    translate([1.5+ radius,0,0])
    for (i=[0:num_holes-1]){
        translate([i*(hole_separation + 2*radius), bloque_dim[1] / 2, 0])
        cylinder(r = radius, h = bloque_dim[2]+3); }
    }

trous(radius, num_holes, hole_separation);
    
difference (){
   bloque(longueur, thinckness, hauteur);
   trous(radius, num_holes, hole_separation);
};

module languette(longueurl, hauteur, thincknessl) {
    cube([longueurl, hauteur, thincknessl]);
}