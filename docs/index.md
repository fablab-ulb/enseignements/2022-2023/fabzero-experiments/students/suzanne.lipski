## Soyez la bienvenue ;

Voici ma page, vous y retrouverez d'une part ma présentation, mais surtout l'avancé de la documentation puisée dans ce cours FabZéro ; apprentissage, doutes, détours et solutions. 

Bonne lecture!


#### Moi, moi, moi

Heyho, 
Je m’appelle Zusanne Lipski, j’ai 21ans et fais des études de bioingénieur à l’ULB. Je suis super mega super forte aux sudokus. Mon chat s’appelle Janette. De nature peu assidue, voire flasque, je ne suis pas encore sure de mon choix d'études. Pour pallier ça, je voyage dès que je peux, je rencontre, pose des questions et dors beaucoup! Je me déplace à vélo et sais faire le doigt d’honneur avec mon pied droit. Je travaille chez Brut by Farm. Je me braque souvent quand je ne comprends pas. Je valorise la justice à l’honneur, l’entraide m’émeus. Je suis thalassophobe mais j’adore nager, le crawl surtout et le papillon quand il y a quelqu’un à impressionner. J’ai découvert il y a quelques mois l’escalade et j’adore ça! Mes amis et ma famille ont une place centrale dans ma vie, j’aime autant susciter le rire chez eux que me retrouver seule. 

Voilà tout ce qui a à savoir sur moi! 

![](https://imagizer.imageshack.com/img924/1328/aUYCyu.jpg)


## Mes expériences...
***...passées*** 

* J'ai eu la chance de participer à [Britains Got Talent en 2009](https://youtu.be/RxPZh4AnWyk)
* Suite à ma prestation, un artiste florissant a composé une chanson en mon nom, n'hésitez pas à aller le soutenir en cliquant juste [ici](https://youtu.be/svitEEpI07E)!


***...futures***

* ???

